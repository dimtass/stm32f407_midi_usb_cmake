/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

#include "stm32f4xx.h"
#include "platform_config.h"
#include "hw_config.h"

// Midi
#include "usbd_usr.h"
#include "usbd_desc.h"

#include "usbd_midi_core.h"
#include "usbd_midi_user.h"

#include "arm_math.h"
/* FFT settings */
#define SAMPLES                    512             /* 256 real party and 256 imaginary parts */
#define FFT_SIZE                SAMPLES / 2        /* FFT size is always the same size as we have samples, so 256 in our case */
 

tp_glb glb = {0};
__ALIGN_BEGIN USB_OTG_CORE_HANDLE  USB_OTG_dev __ALIGN_END;


void main_loop(void)
{
	if (glb.tmr_1ms) {
		glb.tmr_1ms = 0;
//		PIN_LED0_PORT->ODR ^= PI/N_LED1;
//		GPIO_ToggleBits(GPIOF, PIN_LED0 | PIN_LED1);
	}
	if (glb.tmr_1sec >= 1000) {
		TRACE(("glb.tmr_1sec: %d\n", glb.tmr_1sec));
		glb.tmr_1sec = 0;
		GPIO_ToggleBits(GPIOF, PIN_LED0 | PIN_LED1);
	}
}

int main(void)
{    
	// ---------- SysTick timer -------- //
	if (SysTick_Config(SystemCoreClock / 1000)) {
		// Capture error
		while (1){};
	}
	/* set trace levels */
	glb.trace_level =
			0
			| TRACE_LEVEL_DEFAULT
//			| TRACE_LEVEL_USB
			;

	/* Init Data Watchpoint and Trace */
	DWT_Init();

	GPIO_Configuration();

    /* Initialize the midi driver */
	usbd_midi_init_driver(&MIDI_fops);
    /* Make the connection and initialize to USB_OTG/usbdc_core */
	USBD_Init(&USB_OTG_dev,
	            USB_OTG_FS_CORE_ID,
	            &USR_desc,
				&USBD_MIDI_cb,
	            &USR_cb);
	UART1_Configuration();

	TRACE(("Program started.\n"));
	TRACE(("System core clk: %lu\n", SystemCoreClock));

	while(1) {
		main_loop();
	}
}


/**
 * This is a (weak) function in syscalls.c and is used from printf
 * to print data to the UART1
 */
int __io_putchar(int ch)
{
	if ((glb.uart_tx_buff_p.ptr_in + 1)%UART_TX_BUFF_SIZE == glb.uart_tx_buff_p.ptr_out) {
		return ch;
	}

	glb.uart_tx_buff_p.length++;
	glb.uart_tx_buff[glb.uart_tx_buff_p.ptr_in] = ch;
	glb.uart_tx_buff_p.ptr_in = (glb.uart_tx_buff_p.ptr_in + 1)%UART_TX_BUFF_SIZE;

	/* If INT is disabled then enable it */
	if (!glb.uart_tx_buff_p.int_en) {
		glb.uart_tx_buff_p.int_en = 1;
		USART_ITConfig(USART1, USART_IT_TXE, ENABLE); 	// enable the USART1 receive interrupt
	}

	return ch;
}

size_t send_string(uint8_t * buffer, size_t length)
{
	size_t i = 0;
	for (i=0; i<length; i++) {
		__io_putchar(buffer[i]);
	}
	return(i);
}

